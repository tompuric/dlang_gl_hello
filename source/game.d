﻿module game;

import std.stdio;

struct Viewport
{
	ushort width;
	ushort height;
}

class Game
{
	Viewport viewport;
	
	private bool running;
	
	this()
	{
		
	}
	
	void initialize()
	{
		writeln("game.initialize");
	} 
	
	void update()
	{
		
	}
	
	void render()
	{
		
	}
	
	void cleanup()
	{
		
	}
	
	final void run()
	{
		writeln("game.run");
		running = true;
		initialize();
		
		while(running)
		{
			update();
			render();
		}
		
		cleanup();
	}
	
	void stop()
	{
		running = false;
	}
}

