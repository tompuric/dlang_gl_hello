﻿module camera;

import derelict.opengl3.gl3;
import gl3n.linalg;
import gl3n.math;

import std.stdio;

enum Camera_Movement {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT
}

const GLfloat YAW 			= -90.0f;
const GLfloat PITCH			=  0.0f;
const GLfloat SPEED			=  0.25f;
const GLfloat SENSITIVITY	=  0.15f;
const GLfloat ZOOM			=  45.0f;

class Camera
{
	// Camera Attributes
	 vec3 position;
	// viewDirection
	 vec3 front = vec3(0.0f, 0.0f, -1.0f);
	 vec3 up = vec3(0.0f, 1.0f, 0.0f);
	 vec3 right;
	 vec3 worldUp = vec3(0.0f, 1.0f, 0.0f);

	// Eular Angles
	GLfloat yaw;
	GLfloat pitch;

	// Camera options
	GLfloat movementSpeed = SPEED;
	GLfloat mouseSensitivity = SENSITIVITY;
	GLfloat zoom = ZOOM;

	this(vec3 position = vec3(0.0f, 0.0f, 0.0f)
		,GLfloat yaw = YAW
		,GLfloat pitch = PITCH)
	{
		this.position = position;
		this.up = up;
		this.yaw = yaw;
		this.pitch = pitch;
		this.updateCameraVectors();
	}

	this(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch)
	{
		this.position = vec3(posX, posY, posZ);
		this.worldUp = vec3(upX, upY, upZ);
		this.yaw = yaw;
		this.pitch = pitch;
		this.updateCameraVectors();
	}

	mat4 getViewMatrix()
	{
		return mat4.look_at(this.position, this.position + this.front, this.up);
	}

	// Calculates the front vector from the Camera's (updated) Eular Angles
	// It's view point
	private void updateCameraVectors()
	{
		// Calculate the new Front vector
		vec3 front;
		front.x = cos(radians(this.yaw)) * cos(radians(this.pitch));
		front.y = sin(radians(this.pitch));
		front.z = sin(radians(this.yaw)) * cos(radians(this.pitch));
		this.front = front.normalized();
		// Also re-calcuate the Right and Up vector
		this.right = cross(this.front, this.worldUp).normalized();
		this.up = cross(this.right, this.front).normalized();
	}

	void move(Camera_Movement direction, GLfloat deltaTime)
	{
		GLfloat velocity = this.movementSpeed * deltaTime;
		// http://dlang.org/spec/statement.html#FinalSwitchStatement
		final switch (direction) with (Camera_Movement)
		{
			// MOVE FORWARD/BACKWARD
			case FORWARD:
				this.position += this.front * velocity;
				break;
			case BACKWARD:
				this.position -= this.front * velocity;
				break;
			// MOVE LEFT/RIGHT
			case LEFT:
				this.position -= this.right * velocity;
				break;
			case RIGHT:
				this.position += this.right * velocity;
				break;
		}
	}

	void look(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true)
	{
		xoffset *= this.mouseSensitivity;
		yoffset *= this.mouseSensitivity;

		// look left to right
		this.yaw += xoffset;
		// look up to down
		this.pitch += yoffset;
		// roll -> look sideways, (ignore)

		//writeln(yaw, pitch);

		// Make sure that when the pitch is out of bounds, the screen doesn't get flipped
		// A user will not be able to look up and behind.
		// This can be fixed by quaternions
		if (constrainPitch)
		{
			if (this.pitch > 89.0f)
			{
				this.pitch = 89.0f;
			}
			if (this.pitch < -89.0f)
			{
				this.pitch = -89.0f;
			}
		}

		//writeln(getViewMatrix());

		// Update front, right and up vectors using their updated eular angles
		this.updateCameraVectors();
	}

	void camera_zoom(GLfloat yoffset)
	{
		if (this.zoom >= 1.0f && this.zoom <= 45.0f)
		{
			this.zoom -= yoffset;
		}
		if (this.zoom <= 1.0f)
		{
			this.zoom = 1.0f;
		}
		if (this.zoom >= 45.0f)
		{
			this.zoom = 45.0f;
		}
	}

}

