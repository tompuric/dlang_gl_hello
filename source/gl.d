﻿module gl;

import std.string;
import std.stdio;
import std.conv;
import std.exception;
import derelict.opengl3.gl3;

import std.file;

import std.utf;
import std.algorithm;
import std.array;

enum ShaderType: GLuint
{
	vertex = GL_VERTEX_SHADER,
	geometry = GL_GEOMETRY_SHADER,
	fragment = GL_FRAGMENT_SHADER,
}

class Shader
{
	GLint success;
	GLchar[512] infoLog;
	GLuint shader;

	string vertexShader =
		"#version 330 core\n" ~
		"layout(location = 0) in vec3 position;\n" ~
		"void main()\n" ~
		"{\n" ~
			"gl_Position = vec4(position.xyz, 1.0);\n"~
		"}"
	;

	string fragmentShader =
		"#version 330 core\n" ~
		"out vec4 colour;\n" ~
		"void main()\n" ~
		"{\n" ~
			"colour = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n" ~
		"}"
	;


	this(ShaderType type, string filename)
	{
		auto shaderContents = cast(string) (cast(ubyte[]) readText("./res/shaders/" ~ filename)).filter!(a => a < 128).array;
	
		shader = glCreateShader(type);

		int shaderSourceLength = shaderContents.length.to!int;
		const(char*) shaderSourcePtr = shaderContents.ptr;
		glShaderSource(shader, 1, &shaderSourcePtr, &shaderSourceLength);
		glCompileShader(shader);

		glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(shader, 512, &shaderSourceLength, infoLog.ptr);
			printf("Error compiling shader of type %i, with error: %s", type, infoLog.ptr);
		}

	}

	~this()
	{
		glDeleteShader(shader);
	}

}

class Program
{
	GLint success;
	GLchar[512] infoLog;
	GLuint shaderProgram;

	this(Shader[] shaders)
	{
		shaderProgram = glCreateProgram();

		foreach(s; shaders)
		{
			glAttachShader(shaderProgram, s.shader);
			delete s;
		}

		glLinkProgram(shaderProgram);

		GLint success;
		glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);

		if (!success)
		{
			int i = 0;
			glGetProgramInfoLog(shaderProgram, 512, &i, infoLog.ptr);
			printf("Error compiling and linking program with error: %s", infoLog.ptr);
		}
	}

	void use()
	{
		glUseProgram(shaderProgram);
	}

	GLuint opIndex(string name)
	{
		return glGetUniformLocation(shaderProgram, name.toStringz);
	}

	~this()
	{
		glDeleteProgram(shaderProgram);
	}
}


