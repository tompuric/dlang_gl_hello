﻿module sdl;

import derelict.opengl3.gl3;
import derelict.sdl2.sdl;
import derelict.sdl2.image;
import derelict.assimp3.assimp;

import gl3n.linalg;
import gl3n.math;

import soil;

import game;


import std.exception;
import std.stdio;
import std.string;
import std.math;

import std.path;
import std.file;
import std.conv;

import std.datetime;

import gl;
import camera;



import glObject;

class SDLGame : Game
{
	private SDL_Window *window;
	private SDL_GLContext glContext;

	private SDL_Surface *surface;

	private Shader[] shaders;
	private Program lightingProgram, lampProgram;


	// GL
	GLuint gProgramID = 0;
	GLint gVertexPos2DLocation = -1;
	// GL

	GLuint VAO, VBO, EBO;
	GLuint texture, texture2;
	GLfloat[] buffer_data, buffer_data2;
	GLuint[] element_data;


	bool[1024] keys;
	GLfloat lastX = 400, lastY = 300;
	GLfloat yaw = 0.0f;
	GLfloat pitch = 0.0f;
	bool leftClickHeld = false;

	Camera camera = new Camera(vec3(0.0f, 0.0f, 3.0f));

	glObject cube, plane, lamp;

	vec3[] cubePositions =
	[
		vec3( 0.0f,  0.0f,  0.0f), 
		vec3( 2.0f,  5.0f, -15.0f), 
		vec3(-1.5f, -2.2f, -2.5f),  
		vec3(-3.8f, -2.0f, -12.3f),  
		vec3( 2.4f, -0.4f, -3.5f),  
		vec3(-1.7f,  3.0f, -7.5f),  
		vec3( 1.3f, -2.0f, -2.5f),  
		vec3( 1.5f,  2.0f, -2.5f), 
		vec3( 1.5f,  0.2f, -1.5f), 
		vec3(-1.3f,  1.0f, -1.5f) 
	];


	this()
	{
		viewport.height = 720;
		viewport.width = 1280;
	}

	shared static this()
	{
		DerelictGL3.load();
		DerelictSDL2.load();
		DerelictASSIMP3.load();
	}

	override void initialize()
	{
		super.initialize();
		initWindow();
		initVertices();
		initVao();
		initUV();
		initTextures();
		initShaders();
		initProgram();
		initAttributesUniforms();
	}

	void initWindow()
	{
		writeln("sdlgame.this");
		enforce(SDL_Init(SDL_INIT_EVERYTHING) == 0, "Unable to initialize SDL");
		
		// https://wiki.libsdl.org/SDL_GL_SetAttribute
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3); // OpenGL context major version
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1); // OpenGL context minor version
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); // Type of GL context (Core, Compatibility, ES)
		
		// FSAA multisampling x16
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16); // the number of samples used around the current pixel used for multisample anti-aliasing; defaults to 0
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 16); // the number of buffers used for multisample anti-aliasing; defaults to 0
		
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 32); // the minimum number of bits in the depth buffer; defaults to 16
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); // whether the output is single or double buffered; defaults to double buffering on
		
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG, GL_TRUE); // makes mac happy apparently (only for 3.0 and 3.1)
		
		window = SDL_CreateWindow("Title".toStringz,
			SDL_WINDOWPOS_CENTERED, 
			SDL_WINDOWPOS_CENTERED, 
			viewport.width,
			viewport.height,
			SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		enforce(window, "Unable to create SDL window");
		
		/* Create our opengl context and attach it to our window */
		glContext = SDL_GL_CreateContext(window);
		enforce(glContext !is null, "Unable to create GL context");
		
		writeln(to!string(glGetString(GL_VERSION)));
		
		
		
		
		printf("----------------------------------------------------------------\n");
		printf("Graphics Successfully Initialized\n");
		printf("OpenGL Info\n");
		printf("    Version: %s\n", glGetString(GL_VERSION));
		printf("     Vendor: %s\n", glGetString(GL_VENDOR));
		printf("   Renderer: %s\n", glGetString(GL_RENDERER));
		printf("    Shading: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
		printf("----------------------------------------------------------------\n");
		
		/* This makes our buffer swap syncronized with the monitor's vertical refresh */
		enforce(SDL_GL_SetSwapInterval(1) >= 0, "Unable to set VSync");
		
		//5
		DerelictGL3.reload(GLVersion.GL33);


		// If enabled, do depth comparisons and update the depth buffer.
		glViewport(0, 0, viewport.width, viewport.height);
		glEnable(GL_DEPTH_TEST);



	}

	void initVertices()
	{	
		GLfloat[] vertices2_2 = [
			// Positions          // Colors           // Texture Coords
			// front
			1.0f,  1.0f, 1.0f,   1.0f, 0.0f, 0.0f,   2.0f, 2.0f,   // Top Right
			1.0f, -1.0f, 1.0f,   0.0f, 1.0f, 0.0f,   2.0f, 0.0f,   // Bottom Right
			-1.0f, -1.0f, 1.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,//,   // Bottom Left
			-1.0f,  1.0f, 1.0f,   1.0f, 1.0f, 0.0f,   0.0f, 2.0f,    // Top Left 
			// back
			1.0f,  1.0f, -1.0f,   1.0f, 0.0f, 0.0f,   2.0f, 2.0f,   // Top Right
			1.0f, -1.0f, -1.0f,   0.0f, 1.0f, 0.0f,   2.0f, 0.0f,   // Bottom Right
			-1.0f, -1.0f, -1.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,//,   // Bottom Left
			-1.0f,  1.0f, -1.0f,   1.0f, 1.0f, 0.0f,   0.0f, 2.0f    // Top Left 
		];

		GLuint[] indices2 = [  // Note that we start from 0!
			// front
			0, 1, 2,
			2, 3, 0,
			// top
			0, 3, 7,
			7, 4, 0,
			// back
			4, 5, 6,
			6, 7, 4,
			// bottom
			1, 2, 6,
			6, 5, 1,
			// left
			2, 3, 7,
			7, 6, 2,
			// right
			0, 1, 5,
			5, 4, 0
		];

		GLfloat[] vertices = [
			-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  0.0f, 0.0f,
			0.5f, -0.5f, -0.5f,   1.0f, 0.0f, 0.0f,  1.0f, 0.0f,
			0.5f,  0.5f, -0.5f,   1.0f, 0.0f, 0.0f,  1.0f, 1.0f,
			0.5f,  0.5f, -0.5f,   1.0f, 0.0f, 0.0f,  1.0f, 1.0f,
			-0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  0.0f, 1.0f,
			-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  0.0f, 0.0f,
			
			-0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  0.0f, 0.0f,
			0.5f, -0.5f,  0.5f,   1.0f, 0.0f, 0.0f,  1.0f, 0.0f,
			0.5f,  0.5f,  0.5f,   1.0f, 0.0f, 0.0f,  1.0f, 1.0f,
			0.5f,  0.5f,  0.5f,   1.0f, 0.0f, 0.0f,  1.0f, 1.0f,
			-0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  0.0f, 1.0f,
			-0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  0.0f, 0.0f,
			
			-0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f, 0.0f,
			-0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
			-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
			-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
			-0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
			-0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
			
			0.5f,  0.5f,  0.5f,   1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
			0.5f,  0.5f, -0.5f,   1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
			0.5f, -0.5f, -0.5f,   1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
			0.5f, -0.5f, -0.5f,   1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
			0.5f, -0.5f,  0.5f,   1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
			0.5f,  0.5f,  0.5f,   1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
			
			-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
			0.5f, -0.5f, -0.5f,   1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
			0.5f, -0.5f,  0.5f,   1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
			0.5f, -0.5f,  0.5f,   1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
			-0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
			-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
			
			-0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
			0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
			0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
			0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
			-0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
			-0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f, 0.0f, 1.0f
		];

		GLfloat[] vertices2 = [
			1000.0f, -5.0f,  1000.0f,  1.0f, 0.0f, 0.0f,  200.0f, 0.0f,
			-1000.0f, -5.0f,  1000.0f,   1.0f, 0.0f, 0.0f,  0.0f, 0.0f,
			-1000.0f, -5.0f, -1000.0f,   1.0f, 0.0f, 0.0f,  0.0f, 200.0f,

			1000.0f,  -5.0f,  1000.0f,    1.0f, 0.0f, 0.0f,  200.0f, 0.0f,
			-1000.0f, -5.0f, -1000.0f,  1.0f, 0.0f, 0.0f,  0.0f, 200.0f,
			1000.0f,  -5.0f, -1000.0f,  1.0f, 0.0f, 0.0f,  200.0f, 200.0f,
		];

		this.buffer_data = vertices;
		this.buffer_data2 = vertices2;
		this.element_data = indices2;
	}

	void initVao()
	{

	}

	void initUV2()
	{
		// init vao
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);

		glBindVertexArray(VAO);

//		glGenBuffers(1, &EBO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 
			buffer_data.length * GL_FLOAT.sizeof,
			buffer_data.ptr, GL_STATIC_DRAW);


//		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
//		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
//			element_data.length * GL_UNSIGNED_INT.sizeof,
//			element_data.ptr, GL_STATIC_DRAW);

		// Position attribute
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*(GLfloat.sizeof), null);
		glEnableVertexAttribArray(0);

		// Colour Attribute
		//glEnableVertexAttribArray(1);
		//glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8*(GLfloat.sizeof), cast(void*) (3*(GLfloat.sizeof)));

		// TexCoord Attribute
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*(GLfloat.sizeof), cast(void*) (6*(GLfloat.sizeof)));
		glEnableVertexAttribArray(2);

		
		//glDisableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	void initUV()
	{
		cube = new glObject(this.buffer_data);
		plane = new glObject(this.buffer_data2);
		lamp = new glObject(this.buffer_data);
	}

	void initTextures()
	{
		string workDirPath = thisExePath.dirName;//.buildPath("..");
		string textPath = workDirPath.buildPath("res/textures/container.jpg");
		string textPath2 = workDirPath.buildPath("res/textures/awesomeface.png");
		writeln(textPath);
		int width, height;
		ubyte* image_data;
		//ubyte* image_data = SOIL_load_image(textPath.toStringz, &width, &height, &channels, SOIL_LOAD_RGB);

		GLuint texture = SOIL_load_OGL_texture(textPath.toStringz(), 
			SOIL_LOAD_AUTO,
			SOIL_CREATE_NEW_ID,
			SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB
			);

		GLuint texture2 = SOIL_load_OGL_texture(textPath2.toStringz(), 
			SOIL_LOAD_AUTO,
			SOIL_CREATE_NEW_ID,
			SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB
			);

		glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);
		glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image_data);
		writefln("w: %s <> h: %s", width, height);

		//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image_data);
		//glGenerateMipmap(GL_TEXTURE_2D);
		//SOIL_free_image_data(image_data);

		this.texture = texture;
		this.texture2 = texture2;


		// assimp

		string textPath3 = workDirPath.buildPath("res/textures/cube.blend");
		const aiScene *a = aiImportFile(textPath3.toStringz, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_JoinIdenticalVertices);
		writeln("assimp: ", a);
		writeln("assimp: ", a.mNumMeshes);
		writeln("assimp: ", a.mMeshes);
		writeln("assimp: ", a.mMeshes[0].mVertices);
		for (int i = 0; i < a.mMeshes[0].mNumVertices; i++)
		{
			writeln(a.mMeshes[0].mTextureCoords[0][i]);
		}
	}


	void initShaders()
	{
		//shaders = [vertexShader, fragmentShader];
	}

	void initProgram()
	{
		lightingProgram = new Program([
				new Shader(ShaderType.vertex, "lighting/lighting.vs"),
				new Shader(ShaderType.fragment, "lighting/lighting.frag")
		]);

		lampProgram = new Program([
				new Shader(ShaderType.vertex, "lamp/lamp.vs"),
				new Shader(ShaderType.fragment, "lamp/lamp.frag")
		]);
	}

	void initAttributesUniforms()
	{

	}

	void test3()
	{

	}

	void test2()
	{


		long time = SDL_GetTicks();
		int msec = SDL_GetTicks()%20000;
		double val = (sin(msec/5000.0 * 2 * PI) + 1.0f)/2.0f;
		float x = msec/2500.0f;

		// Clear the colorbuffer
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Bind Textures using texture units
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		glUniform1i(lightingProgram["ourTexture"], 0);
		

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture2);
		glUniform1i(lightingProgram["ourTexture2"], 1);




		// Create camera transformations
		mat4 projection = mat4.identity;
		projection = projection.perspective(viewport.width, viewport.height, 45.0f, 0.1f, 1000.0f);
		//projection = projection.orthographic(-10.0f, 10.0f, -10.0f, 10.0f, 0.0f, 100.0f);
		mat4 view = camera.getViewMatrix();
		mat4 model;


		// Lamp Program
		lightingProgram.use();

		// Object one - lighting shader
		GLuint modelLoc = lightingProgram["model"];
		GLuint viewLoc = lightingProgram["view"];
		GLuint projLoc = lightingProgram["projection"];

		glUniform1i(lightingProgram["fTime"], msec);

		glUniform3f(lightingProgram["objectColor"], 1.0f, 0.5f, 0.31f);
		glUniform3f(lightingProgram["lightColor"], 1.0f, 0.5f, 1.0f);

		glUniformMatrix4fv(viewLoc, 1, GL_TRUE, view.value_ptr);//camera.camera.value_ptr);
		glUniformMatrix4fv(projLoc, 1, GL_TRUE, projection.value_ptr);

		glBindVertexArray(cube.getVAO());
		model = mat4.identity;
		glUniformMatrix4fv(modelLoc, 1, GL_TRUE, model.value_ptr);
		cube.render();
		glBindVertexArray(0);

		// Object two - lamp shader

		lampProgram.use();

		// Get location objects for the matrices on the lamp shader (these could be different on a different shader)
		modelLoc = lampProgram["model"];
		viewLoc  = lampProgram["view"];
		projLoc  = lampProgram["projection"];

		// Set matrices
		glUniformMatrix4fv(viewLoc, 1, GL_TRUE, view.value_ptr);
		glUniformMatrix4fv(projLoc, 1, GL_TRUE, projection.value_ptr);
		model = mat4.identity;
		vec3 lightPos = vec3(1.2f, 1.0f, 2.0f);
		model = model.scale(0.2f, 0.2f, 0.2f);
		model = model.translate(lightPos);


		glBindVertexArray(lamp.getVAO());
		glUniformMatrix4fv(modelLoc, 1, GL_TRUE, model.value_ptr);
		lamp.render();
		glBindVertexArray(0);


		//glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, cast(const void*) 0);

		/*
		for (GLuint i = 0; i < 10; i++)
		{
			glBindVertexArray(cube.getVAO());
			// Calculate the model matrix for each object and pass it to shader before drawing
			model = mat4.identity;

//			model = model.rotatez(2*PI/i);
//			model = model.translate(i*2, 0.0f, 0.0f);

			model = mat4.identity;
			model = model.scale(2.0f, 2.0f, 2.0f,);

			GLfloat angle = 20.0f * i;

			model = model.rotatex(angle);

			if (i % 3 == 0)
			{
				model.rotatez(angle*x/100.0f);
			}

			model = model.translate(cubePositions[i]);


			//model = model.rotatey(angle*0.3f);
			//model = model.rotatez(angle*0.5f);

			glUniformMatrix4fv(modelLoc, 1, GL_TRUE, model.value_ptr);
			cube.render();
		}

		mat4 pl = mat4.identity;
		pl = pl.translate(0.0f, -5.0f, 0.0f);
		glUniformMatrix4fv(modelLoc, 1, GL_TRUE, pl.value_ptr);
		plane.render();

		lampShader.use();
		vec3 lightPos = vec3(1.2f, 1.0f, 2.0f);
		mat4 light = mat4.identity;
		light = light.translate(lightPos);
		light = light.scale(0.2f, 0.2f, 0.2f);
		glUniformMatrix4fv(modelLoc, 1, GL_TRUE, light.value_ptr);
		lamp.render();

		*/

		glBindTexture(GL_TEXTURE_2D, 0);

		glBindVertexArray(0);
	}


	override void cleanup()
	{
		super.cleanup;
		writeln("sdl.cleanup");
		SDL_GL_DeleteContext(glContext); 
		SDL_DestroyWindow(window);
		SDL_Quit();
	}

	override void update() 
	{
		super.update;

		SDL_Event event;


		
		while(SDL_PollEvent(&event))
		{
			SDL_Keycode c = event.key.keysym.sym;
			if (event.type == SDL_KEYUP)
			{
				keys[c] = false;
			}
			if (event.type == SDL_KEYDOWN)
			{
				keys[c] = true;
			}
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				writeln("clicked");
				leftClickHeld = true;
				lastY = event.motion.y;
				lastX = event.motion.x;
			}
			if (event.type == SDL_MOUSEBUTTONUP)
			{
				leftClickHeld = false;
			}
			if (event.type == SDL_MOUSEMOTION && leftClickHeld)
			{

				Sint32 x = event.motion.x;
				Sint32 y = event.motion.y;
				GLfloat xoffset = x - lastX;
				GLfloat yoffset = lastY - y; // Reversed since y-coordinates range from bottom to top

				write(lastX, " - ", lastY, " <> ");
				lastX = x;
				lastY = y;
				
				camera.look(xoffset, yoffset);
				writeln(lastX, " - ", lastY, " <> ", xoffset, " - ", yoffset, " <> ", x, " - ", y);
			}

			if (event.type == SDL_QUIT)
			{
				stop();
			}
		}

		GLfloat cameraSpeed = 0.05f;
		float val = 0.5;
		if (keys[SDLK_a]) {
			// left
			camera.move(Camera_Movement.LEFT, 1.0f);
		}
		if (keys[SDLK_d]) {
			// right
			camera.move(Camera_Movement.RIGHT, 1.0f);
		}
		if (keys[SDLK_w]) {
			// up
			camera.move(Camera_Movement.FORWARD, 1.0f);
		}
		if (keys[SDLK_s]) {
			// down
			camera.move(Camera_Movement.BACKWARD, 1.0f);
		}
		if (keys[SDLK_q]) {
			// rot_L
//			camera.move(0.0f, 0.0f, val);
			camera.look(-1.0f, 0.0f);
		}
		if (keys[SDLK_e]) {
			// rot_R
//			camera.move(0.0f, 0.0f, -val);
			camera.look(1.0f, 0.0f);
		}
		if (keys[SDLK_z]) {
			// rot_R
//			camera.rotatex(-val);
			writeln("position", camera.position);
			writeln("front", camera.front);
			writeln("up", camera.up);
			writeln("right", camera.right);
			writeln("worldUp", camera.worldUp);
			writeln(camera.getViewMatrix());
			writeln();
		}
		if (keys[SDLK_x]) {
			// rot_R
//			camera.rotatex(val);
		}
		if (keys[SDLK_c]) {
			// rot_R
//			camera.rotatez(-val);
		}
		if (keys[SDLK_v]) {
			// rot_R
//			camera.rotatez(val);
		}

		SDL_Delay(50);
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		test2();
		test3();

		SDL_GL_SwapWindow(window);
	}

	override void render() 
	{
		super.render;
	}
}

