﻿module glObject;

import derelict.opengl3.gl3;

class glObject
{

	private GLuint objectVAO;
	private GLuint objectVBO;
	private GLfloat[] vertices;

	this(GLfloat[] vertices)
	{
		this.vertices = vertices;

		// Generates 1 VAO
		glGenVertexArrays(1, &objectVAO);

		// Generates 1 Buffer Object
		glGenBuffers(1, &objectVBO);

		// Binds a VAO where any subsequent VBO/EBO,
		// glVertexAttributePointer or glEnableVertexAttributeArray
		// calls will be stored inside this VAO
		glBindVertexArray(objectVAO);

		// Binds a buffer object to the current buffer type ARRAY/ELEMENT
		glBindBuffer(GL_ARRAY_BUFFER, objectVBO);
		// Allocates memory and stores data in the currently binded Buffer Object
		glBufferData(GL_ARRAY_BUFFER, vertices.length * GL_FLOAT.sizeof, vertices.ptr, GL_STATIC_DRAW);

		// Vertex Coords
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * GLfloat.sizeof, cast(void*) 0);

		// Colour Coords
//		glEnableVertexAttribArray(1);
//		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * GLfloat.sizeof, cast(void*) (3 * GLfloat.sizeof));

		// Texture Coords
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * GLfloat.sizeof, cast(void*) (6 * GLfloat.sizeof));

		// close VBO
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// close VAO
		glBindVertexArray(0);
	}

	GLuint getVAO()
	{
		return objectVAO;
	}

	GLuint getVBO()
	{
		return objectVBO;
	}

	void render()
	{
		glDrawArrays(GL_TRIANGLES, 0, cast(int) vertices.length);
	}
}

