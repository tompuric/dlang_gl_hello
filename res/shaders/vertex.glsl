#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 colour;
layout (location = 2) in vec2 texCoord;

out vec3 vertexColour;
out vec2 vertexTexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    
    gl_Position = projection * view * model * vec4(position.x, position.y, position.z, 1.0f);
    vertexColour = colour;
    if (vertexColour.xyz == vec3(0, 0, 0)) {
        vertexColour = vec3(1.0f, 1.0f, 1.0f);
    }
    vertexTexCoord = texCoord;
}