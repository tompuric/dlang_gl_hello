#version 330 core

in vec3 vertexColour;
in vec2 vertexTexCoord;

out vec4 colour;

uniform int fTime;

uniform sampler2D ourTexture;
uniform sampler2D ourTexture2;

void main()
{
  	float aux = 0.0;
  	float PI = 3.141592653589793238462643383;
  	float temp = (0 * (1 - fTime) + 1/2512.0f * fTime)*4.0f;
  	//float temp2 = (((((-2*PI) / 2*PI) + 3*PI) / 2*PI) - PI) * fTime;

  	float temp3 = (sin(mix(0, 2*PI, fTime/5000.0f)) + 1.0f)/2.0f;

  	float temp4 = (sin(fTime/5000.0 * 2 * PI) + 1.0f)/2.0f;
    aux = 1 - sin(temp);

  	colour = mix(texture(ourTexture, vertexTexCoord), texture(ourTexture2, vertexTexCoord), temp3) * vec4(vertexColour, 1.0f);
}